## Pekka Paalanen


### Hardware

* At home (cooler dropped off): GeForce 256, AGP, VGA 
* At home: nv20 10de:0200, GeForce3, Elsa Gladiac 920, AGP, VGA 
* At home (installed): nv28 10de:0282, GeForce4 TI4800 SE, AGP, DVI+VGA 
* Work laptop: NVD9 (Thinkpad T420, with a BIOS switch to select between Intel, Nvidia, and Optimus.) 

### Links

* home page: [[http://www.iki.fi/pq/|http://www.iki.fi/pq/]] 
* my related files: [[http://jumi.lut.fi/~paalanen/scratch/|http://jumi.lut.fi/~paalanen/scratch/]] 
* my mmio-trace git repo: [[http://cgit.freedesktop.org/~pq/mmio-trace/|http://cgit.freedesktop.org/~pq/mmio-trace/]] 
* [[http://cgit.freedesktop.org/~pq/nv20_demo/|http://cgit.freedesktop.org/~pq/nv20_demo/]] 
Contact: find me in irc channel #nouveau in freenode, nick pq. 


#### To Read

* [[TTMMemoryManager|TTMMemoryManager]] 
* [[http://lkml.org/lkml/2007/4/26/74|http://lkml.org/lkml/2007/4/26/74]] TTM discussion 
* [[http://lkml.org/lkml/2007/8/19/155|http://lkml.org/lkml/2007/8/19/155]] cached and uncached buffers, performance 
* [[http://www.linuxjournal.com/article/5783|http://www.linuxjournal.com/article/5783]] Writing Portable Device Drivers by Greg K.-H. 
* [[http://tomayko.com/writings/the-thing-about-git|http://tomayko.com/writings/the-thing-about-git]] 

### Git 1.5

If you have repositories created or cloned with earlier git version than 1.5, these instructions may not apply. 


#### Setting up DRM for pushing

The mesa/drm tree has lots and lots of branches, tracking them all is a bit useless, so I used this recipe found in `man git-remote`: 
[[!format txt """
$ mkdir drm.git
$ cd drm.git
$ git-init
$ git-remote add -f -t master -m master origin ssh://git.freedesktop.org/git/mesa/drm
$ git merge origin
"""]]
This will get me the `origin/master` branch and nothing else. I can later add new branches to be tracked by e.g. 
[[!format txt """
$ git-config --add remote.origin.fetch +refs/heads/modesetting-101:refs/remotes/origin/modesetting-101
$ git-fetch origin
"""]]
Now I can check the tracked branches with `git-branch -r` and config settings with `git-config -l`. To be warm and fuzzy (make sure where my git-push goes) I do 
[[!format txt """
$ git-config --add remote.origin.push master:master
"""]]
which probably is some kind of default, but does not hurt to define it anyway. I am going to push my master branch, so I must remember to do my development in other branches 

To keep those nasty trailing whitespaces from my commits, I should remember to activate `.git/hooks/pre-commit` script by making it executable. 

And remember, **do not use** `git-pull` when `git-fetch && git-rebase origin` can do the job. Spurious merge commits are annoying. 


#### New git repo at fd.o

[[http://www.freedesktop.org/wiki/Infrastructure/git/RepositoryAdmin|http://www.freedesktop.org/wiki/Infrastructure/git/RepositoryAdmin]] 
[[!format txt """
git remote add public ssh://people.freedesktop.org/~pq/nv20_demo
git-config remote.public.push master:master
git push --force public
"""]]

### Testing patches against git revisions in Gentoo

Using git ebuilds lets one test properly formatted patches easily. Say, you filed a bug against a git master HEAD some time ago, and now you got a patch to try. First find out which revision you actually used, since master HEAD has moved. It can be seen from emerge per-package-install log if git fetch was actually downloading something, or going to `/var/portage/distfiles/git-src/*` and inspecting the repo. Then just e.g.: 

      * EGIT_PATCHES="/home/pq/tmp/0001-dix-don-t-apply-button-mappings-to-SDs-or-the-devi.patch" EGIT_TREE=734b23e5982e171031077a2d5d6b5dc2a12e1a70 emerge -va xorg-server 

### Claws-mail

The script [[claws-git-am.bash|http://jumi.lut.fi/~paalanen/scratch/claws-git-am.bash]] can be set as an Action in claws-mail. Then select some patch email and use the Action to apply them directly from claws-mail. 
