[[!table header="no" class="mointable" data="""
 [[Accueil|FrontPage-fr]]  |  [[TiNDC 2006|IrcChatLogs-fr]]  |  [[TiNDC 2007|IrcChatLogs-fr]]  |  [[Archives anciennes|IrcChatLogs-fr]]  |  [[Archives récentes|IrcChatLogs-fr]]  | DE/[[EN|Nouveau_Companion_25]]/ES/[[FR|Nouveau_Companion_25-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Le compagnon irrégulier du développement de Nouveau (TiNDC)


## Édition du 8 août 2007


### Introduction

Un peu plus de 2 semaines ont passé et j'ai depuis reçu des menaces des développeurs principaux afin de m'extorquer un nouveau TiNDC (Si tu n'écris pas, nous ne codons pas !). Pour le salut du projet et le bien de la communauté, je me suis dévoué : voici donc l'édition 25 du TiNDC. 

Avant de débuter, je voudrait remercier ahuillet qui, une fois encore, a pris le temps de corriger mon brouillon au sujet de Xv. Et maintenant... 


### Statut actuel

La plupart des discussions ont été à propos de comment se débarrasser de l'attente active de la notification DMA. Après avoir eu l'avis de darktama, ajax, pq et marcheu, une tendance s'est matérialisée : éviter TTM pour l'instant et implémenter un schéma avec 2 ioctl()s :     

1. Allouer un nouveau numéro de _fence_ (uint64_t qui augmente toujours) 
1. Attendre le _fence_ numéro #, 
avec le système partant du principe que les transferts sont effectués dans l'ordre et que le numéro de fence soit valable pour chaque fifo, cela aurait pu marcher. 

Parce que TTM ne répond pas parfaitement à nos besoins (par exemple, la gestion des FIFO depuis l'espace utilisateur), il semble que de plus en plus souvent, Nouveau utilisera seulement quelques fonctionnalités de celui-ci, en implémentant les fonctionnalités manquantes à côté. 

Puisque TTM doit marcher pour un grand nombre de cartes différentes, il marche également même pour la plus bête de toutes cartes. Et parce que NVidia a déjà implémenté la majorité des fonctionnalités au niveau matériel, la plupart du code de TTM est inutile et superflu pour nous. 

Cette décision ne fut pas du tout du goût de Airlied (le mainteneur DRM) qui voudrait avoir un seul gestionnaire de mémoire (TTM, remanié si besoin). Les discussions se sont déroulées sur le canal IRC #dri-devel ([[http://dri.egore911.de/index.php?date=2007-07-26|http://dri.egore911.de/index.php?date=2007-07-26]]) avec des points valides échangés des deux côtés. La décision finale est que nous allons migrer à TTM, mais puisque TTM n'est pas stable, ni totalement utilisé dans les drivers actuels (au plus il y a deux drivers qui utilisent TTM ou qui marchent avec : Radeon et Intel), et qu'il manque des fonctionnalités pour Nouveau ; nous allons travailler avec les gars de TTM et allons l'adapter à nos besoins, d'ici-là nous ajouterons du code pour boucher les trous afin que Nouveau marche. 

Évitant ce champ de mines de débats "politiques", ahuillet a changé ses priorités et essaye maintenant d'implémenter le _double buffering_. Moins de 24 heure plus tard, il cherchait déjà des testeurs. 

Pour rendre les choses un peu plus complexe, darktama a travaillé sur l'intégration de TTM dans Nouveau. Il a préparé un patch et l'a envoyé à dri-devel pour vérification et approbation, puisqu'il modifiait quelques parties de TTM pour mieux correspondre à nos besoins. Pendant qu'il attendait une réponse, la discussion mentionnée plut tôt s'est déchaînée. 

Le travail sur Xv a continué, avec p0g écrivant quelques tests pour mieux comprendre comment les cartes gèrent les données en YV12. 

Après avoir regardé les résultats de p0g, pq, ahuillet et marcheu en sont venu à la conclusion que le format natif était du nv12 (c'est un format de codage / d'affichage, pas une carte NVidia, jetez un oeuil à www.fourcc.org "YUV Formats"). De plus, les données à afficher étaient manifestement envoyées à la carte par une FIFO et non par transfert DMA. D'autres tests ont donc dû être faits pour trouver pourquoi, et quel type de performance cela pouvait nous apporter en plus. Les résultats ne sont toujours pas tombés. 

Un autre sujet intéressant à propos de Xv est l'objet _overlay_. Une fois que le travail initial sur Xv a marché, p0g et ahuillet ont essayé de comprendre les données envoyées vers la carte lors de l'utilisation d'un overlay. Ils ont utilisé des traces valgrind-mmt et ont trouvé les objets et comment contrôler la plupart des attributs. 

L'objet _overlay_ n'est pas un objet normal comme ceux de NV30_TCL_PRIMITIVE_3D ou les objets PCI / AGP mentionnés lors des éditions précédentes. C'est un objet logiciel, ce qui veut dire que si vous écrivez une valeur à un endroit dans cet objet, la carte génèrera une interruption qui sera alors gérée par le module noyau. La module noyau doit alors regarder la valeur écrite et exécuter une fonction assignée à cette valeur. La fonction va alors écrire / gérer les registres MMIO qui vont alors délivrer la fonctionnalité requise à l'écran. Puisque cela n'était pas ce que ahuillet cherchait / s'attendait à trouver, le travail s'est déplacé sur d'autres sujets. 

Mais sur ce travail sur l'overlay (mettant en jeu des traces MMIO), marcheu et ahuillet en sont venu à la conclusion que nous pourrions supporter nativement le YV12 (en fait, le NV12, mais la conversion est simple...) en affectant simplement les mêmes valeurs aux registres que l'overlay, mais il s'est avéré que cela n'était pas si facile. Affecter ces deux bits a donné des résultats, et un de ceux-ci a laissé penser à ahuillet que nous avions touché le jackpot, mais débugger des images mal affichées n'est pas si facile, cela a donc pris plusieurs heures pour être seulement sûr à 80% que cela n'était pas le bit pour le NV12 natif, mais plutôt un paramètre quelconque pour la conversion YUV->RGB. 

Donc pour l'instant nous pouvons dire : pour toutes les cartes <NV50 nous devrions avoir une implémentation de Xv fonctionnelle qui est un peu plus performante que la précédente. En passant : marcheu et ahuillet ont effectué quelques benchmarks et le point intéressant est que Xv est relativement rapide pour une _frame_ donnée, mais lorsque mplayer affiche ces _frames_, il affiche des compteurs à l'écran, et cela provoque des opérations 2D, qui à leur tour synchronisent tout, y compris le transfert Xv. 

À propos de renouveau : pmdata est toujours en train de séparer renouveau en un _dumper_ et un parseur. Il a réussi à créer des _dumps_ contenant uniquement les valeurs depuis la mémoire parsée, et les faire passer plus tard à traver un parteur qui affiche les _dumps_ comme nous les connaissons actuellement. Cependant il manque encore le framework xml. 

Un autre problème important a été _bisecté_ par pq, ce qui n'était pas facile du tout : Lors de la visualisation de 2 vidéos par Xv sur nouveau, la file DMA se bloquait tôt ou tard (après 10 minutes au plus tard). Puisque ces problèmes ne se produisaient pas sous nv, il a poursuivi le problème jusqu'à un _commit_ combiné dans DDX et DRM ([[https://bugs.freedesktop.org/show_bug.cgi?id=11820|https://bugs.freedesktop.org/show_bug.cgi?id=11820]]). 

Pendant le weekend, airlied a effectué un peu de travail sur PPC. Après quelques problèmes dûs à un fichier nouveau_drm.h pas à jour, il a réussi à faire marcher glxgears à nouveau sur son G5. Cependant, les couleurs sont toujours mauvaises (noir), mais c'est un peu mieux que si ça ne marchait pas du tout. 

De plus, airlied a fait marcher renouveau sous MacOS X, donc nous pouvons également avoir des informations sur la gestion des cartes nvidia sur PPC. 

||<style="background-color: #F0EFE0;">"Je devrait probablement simplifier des grosses parties de renouveau avec une hache..."  
  
' "a sorti Airlied rêvant d'une architecture logicielle géniale qui faciliterait le portage." || 

En plus du travail de ahuillet sur Xv, jb17some a travaillé sur XvMC. Il est presque certain d'avoir trouvé la manière dont les données sont transférées à la carte et comment ces données sont alors modifiées avant l'envoi final. Un résumé plus à jour sur ses découvertes sont sur le wiki : [[http://nouveau.freedesktop.org/wiki/jb17bsome|http://nouveau.freedesktop.org/wiki/jb17bsome]] . 

Marcheu et Darktama on aussi fait quelques corrections : ils ont mis à jour DRM en version 0.0.10 qui supprime la gestion des sous-canaux par le développeur écrivant le driver et le fait gérer à la place par le DRM. Le DRM également a son propre sous-canal, qui n'est pas utilisé pour l'instant. Cependant, il sera bientôt requis pour avoir TTM qui marche avec Nouveau. Et cela conclut l'histoire de l'utilisation ou non de TTM. 

"Un canal est juste un autre mot pour FIFO. Chaque FIFO a 8 sous-canaux qui sont des slots contenant une commande à exécuter par la carte. Ces slots sont exécutés dans l'ordre." 

Maintenant, pourquoi le DRM a besoin de sa propre FIFO? Eh bien, dans le futur, le TTM va gérer la mémoire des cartes. La philosophie de TTM c'est que le DRM est propriétaire des morceaux de mémoire et les place comme il le veut, donc il a aussi besoin d'être capable de déplacer des choses où il veut afin de pouvoir éviter la fragmentation mémoire. Il faut donc être capable d'effectuer du DMA, et c'est pour cela qu'il faut un objet DMA dont nous avons parlé dans les éditions précédentes. Et pour cela il faut une FIFO. 

L'étape suivant dans le TODO de Marcheu est de faire marcher la fonctionnalité "mise à jour vers l'écran" sur NV3x et alors d'étendre les fonctionnalités Exa des NV40 aux NV3x. Après cela du travail sur le dual-head viendra sûrement. L'ordre dans laquelle les fonctionnalités sont implémentées est en partie basé sur leur intérêt et sur la progression du TTM (voir plus haut). Pour l'instant, l'état du TTM fait que le travail sur la 3D est difficile. On s'oriente donc sur les parties "plus simples" comme les performances 2D. 

Cependant, utiliser des sous-canaux demande que les changement de contexte marchent, mais quelques cartes (NV1x et NV2x) ont des problèmes tels que des blocages des files DMA pendant le démarrage de X. Un travail plus poussé est requis (et des testeurs sont les bienvenus!) mais on dirait que le changement de contexte ne marche pas totalement sur ces cartes. 

Et comme ça a souvent été le cas lorsque quelque chose ne marche plus, matc vient à la rescousse. Cette fois-ci il s'est aperçu qu'il n'initialisait pas correctement les interruptions des cartes NV3x (en réalité, il est correctement fait dans un premier temps, mais d'autre procédures d'initialisation les désactivent plus tard sans les réactiver par la suite) 


### Aide requise

Ahuillet aimerait que plus de monde teste le code de Xv. En plus de compiler et installer Nouveua, cela implique surtout de regarder des vidéos et de remonter le résultat. :) 

Nous apprécierions que des propriétaires de 8800 testent le pilote actuel et nous fassent part du résultat. Nous n'avons actuellement que deux cartes G84 pour développer et tester, et le retour d'autres utilisateurs de ces cartes serait bienvenue. Note : utilisez la branche randr-1.2 et faire remonter les résultats à Darktama. 

Et nous aurions besoin de dump [[MmioTrace|MmioTrace-fr]] pour les cartes NV41, NV42, NV44,NV45, NV47,NV48 et NV4C. Faites vous connaitre sur le canal si vous pouvez nous aider. 

Finalement, une correction par rapport aux nombre d'accès publiés la semaine dernière, je m'étais trompé, mes statistiques montraient les accès absolus et non pas les références. Donc le nombre d'accès était à peu près 3 fois trop haut! Nous sommes pour l'instant à environ 2200 accès pour l'édition n°23. 

[[<<< Édition précédente|Nouveau_Companion_24-fr]] | [[Édition suivante >>>|Nouveau_Companion_26-fr]] 
